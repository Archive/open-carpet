###
### oc_channelconf.py
###
### Copyright (C) 2003, 2004 Ximian, Inc.
###
### This program is free software; you can redistribute it and/or
### modify it under the terms of version 2 of the GNU General Public
### License as published by the Free Software Foundation.
###
### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.
### 
### You should have received a copy of the GNU General Public License
### along with this program; if not, write to the Free Software
### Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
### USA.

import string, sys, gzip, os
import redcarpet

import oc_conffile, oc_util

class ChannelConf(oc_conffile.ConfFile):

    def __init__(self):
        oc_conffile.ConfFile.__init__(self)

        self.add_known_key("ChannelID", required=1)
        self.add_known_key("Name", required=1)
        self.add_known_key("Alias")
        self.add_known_key("Description")
        self.add_known_key("Priority")
        self.add_known_key("IconPath")
        self.add_known_key("PackagePath")


    def pre_process(self):
        self.__channel_xml = []


    def store_channel_info(self, channel_info):
        self.validate_required_keys()

        if self.has_key("IconPath"):
            icon_path = self.relative_path(self.get("IconPath"))
        else:
            icon_path = None

        props = (
            ( "id",                 self.get("ChannelID")             ),
            ( "name",               self.get("Name")                  ),
            ( "alias",              self.get("Alias")                 ),
            ( "description",        self.get("Description")           ),
            ( "priority",           self.get("Priority")              ),
            ( "pub",                "1"                               ),
            ( "icon",               icon_path                         ),
            )

        props = props + channel_info
                  
        def prop_to_str(x):
            if x is None:
                return ""
            key, val = x
            if key is None or val is None:
                return ""
            return '%s="%s"' % (key, val)
        props_str = string.join(map(prop_to_str, props), " ")

        self.__channel_xml.append("<channel %s/>\n" % props_str)


    def store_helix_channel_info(self, distro, pkginfo_file):
        pkginfo_file = self.relative_path(pkginfo_file)
        pkg_dir      = os.path.dirname(pkginfo_file)
        pkginfo_file = os.path.basename(pkginfo_file)

        channel_info = (
            ( "type",          "helix"      ),
            ( "distro_target", distro       ),
            ( "path",          pkg_dir      ),
            ( "pkginfo_file",  pkginfo_file ),
            )
            
        self.store_channel_info(channel_info)
    

    def store_apt_channel_info(self, distro, apt_root, apt_repo_name):
        apt_root = self.relative_path(apt_root)

        if apt_root[-1] != '/':
            apt_root = apt_root + "/"

        channel_info = (
            ( "type",           "aptrpm"                                  ),
            ( "distro_target",  distro,                                   ),
            ( "path",           apt_root + "base",                        ),
            ( "file_path",      "%sRPMS.%s/" % (apt_root, apt_repo_name), ),
            ( "pkginfo_file",   "pkglist.%s.bz2" % apt_repo_name          ),
            )

        self.store_channel_info(channel_info)

    def store_yum_channel_info(self, distro, yum_root, yum_repo_name):
        yum_root = self.relative_path(yum_root)

        channel_info = (
            ( "type",           "yum"                                     ),
            ( "distro_target",  distro,                                   ),
            ( "path",           yum_root,                                 ),
            ( "file_path",      yum_root,                                 ),
            ( "pkginfo_file",   "%s/headers/header.info" % yum_root       ),
            )

        self.store_channel_info(channel_info)

    def write_pkginfo(self, pkg_path):

        norm_pkg_path = self.normalize_path(pkg_path)
        packages = oc_util.get_packages(norm_pkg_path)

        pkginfo_file = os.path.join(pkg_path, "packageinfo.xml.gz")
        pkginfo_file = self.normalize_path(pkginfo_file)

        fh = gzip.open(pkginfo_file, "wb")

        fh.write("<?xml version=\"1.0\"?>\n\n")
        fh.write("<channel>\n")
        fh.write("<subchannel name=\"subchannel\" priority=\"100\">\n")

        for pkg in packages:
            fh.write(pkg.to_xml())
            fh.write("\n")

        fh.write("</subchannel>\n")
        fh.write("</channel>\n")

        return pkginfo_file
            

    def process_adddistro(self, val):
        args = map(string.strip, val.split(" "))

        if len(args) == 0:
            self.error("Too few arguments to AddDistro")
        elif len(args) == 1:
            distro = args[0]
            pkg_path = args[0]
        elif len(args) == 2:
            distro = args[0]
            pkg_path = args[1]
        else:
            self.error("Too many arguments to AddDistro")

        pkginfo_file = self.write_pkginfo(pkg_path)
        self.store_helix_channel_info(distro, pkginfo_file)

    def process_addaptdistro(self, val):
        args = map(string.strip, val.split(" "))

        if len(args) < 3:
            self.error("Too few arguments to AddAptDistro")
        elif len(args) > 3:
            self.error("Too many arguments to AddAptDistro")

        distro, apt_root, apt_repo_name = args
        self.store_apt_channel_info(distro, apt_root, apt_repo_name)

    def process_addyumdistro(self, val):
        args = map(string.strip, val.split(" "))
        if len(args) < 3:
            self.error("Too few arguments to AddYumDistro")
        elif len(args) > 3:
            self.error("Too many arguments to AddYumDistro")

        distro, yum_root, yum_repo_name = args
        self.store_yum_channel_info(distro, yum_root, yum_repo_name)

    def post_process(self):
        pass


    def get_channel_xml(self):
        return self.__channel_xml

