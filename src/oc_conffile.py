###
### oc_conffile.py
###
### Copyright (C) 2003, 2004 Ximian, Inc.
###
### This program is free software; you can redistribute it and/or
### modify it under the terms of version 2 of the GNU General Public
### License as published by the Free Software Foundation.
###
### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.
### 
### You should have received a copy of the GNU General Public License
### along with this program; if not, write to the Free Software
### Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
### USA.

import sys, string, os

class ConfFile:

    def __init__(self):
        self.__known_keys = {}
        self.__required_keys = {}
        self.__conf = {}

        self.__root   = None
        self.__offset = None

        self.__basename = "?"
        self.__line_no = -1


    def add_known_key(self, key, required=0):
        key = key.strip().lower()
        self.__known_keys[key] = 1

        if required:
            self.__required_keys[key] = 1
        
    def validate_required_keys(self):
        for key in self.__required_keys:
            if self.get(key) is None:
                self.error("Required key '%s' missing from "
                           "configuration file" % key)

    def message(self, txt):
        if self.__line_no >= 0:
            num = str(self.__line_no)
        else:
            num = "?"
        sys.stderr.write("%s:%s:%s\n" % (self.__basename, num, txt))
        

    def warning(self, txt):
        self.message("Warning: "+txt)
        

    def error(self, txt):
        self.message("ERROR: " + txt)
        sys.exit(-1)
        

    def pre_process(self):
        pass
    

    def post_process(self):
        pass
    

    def read(self, filename, default_filename=None, parent=None):

        if not filename:
            filename = default_filename
        elif default_filename and os.path.isdir(filename):
            filename = os.path.join(filename, default_filename)

        if not os.path.exists(filename):
            raise IOError, "Can't find '%s'" % filename

        if not os.path.isfile(filename):
            raise IOError, "'%s' is not a file" % filename

        if not os.access(filename, os.R_OK):
            raise IOError, "Can't read file '%s'" % filename

        # Initialize root and offset
        dirname = os.path.dirname(filename)
        if parent:
            self.__root = parent.__root
            # +1 trims off separator
            self.__offset = dirname[len(self.__root)+1:]
        else:
            self.__root = dirname
            self.__offset = ""

        f = file(filename)

        self.__basename = os.path.basename(filename)

        unknown = []
        line_no = 0
        for line in f.xreadlines():

            line_no += 1

            # drop empty lines and comment lines
            line = line.strip()
            if not line or line[0] == "#":
                continue

            # keys w/o values get the default value of "On"
            pieces = map(string.strip, line.split(" ", 1))
            key = pieces[0].lower()
            if len(pieces) == 2:
                val = pieces[1]
            else:
                val = "On"

            # strip quotes
            if val[0] == '"' and val[-1] == '"' and len(val) > 1:
                val = val[1:-1]

            if self.__known_keys.has_key(key):
                self.__conf[key] = val
            else:
                # If we don't know what to do with a key, add it and its
                # value to the unknown list for subsequent processing.
                unknown.append((key, val, line_no))

        self.pre_process()

        # Walk across the list of unknown keys and check for handler
        # methods.
        for key, val, line_no in unknown:
            self.__line_no = line_no
            handler_method = "process_" + key
            if hasattr(self, handler_method):
                fn = getattr(self, handler_method)
                if callable(fn):
                    fn(val)
                else:
                    self.error("Invalid handler '%s' (key='%s', "
                               "value='%s')\n" % (handler_method, key, val))

            else:
                self.warning("Ignoring unknown key '%s' "
                             "(value='%s')\n" % (key, val))

        self.post_process()
                


    def has_key(self, key):
        key = key.strip().lower()
        return self.__conf.has_key(key)


    def get(self, key, default=None):
        key = key.strip().lower()
        return self.__conf.get(key, default)


    def set(self, key, val):
        key = key.strip().lower()
        self.__conf[key] = val


    def is_true(self, key, default=None):
        val = self.get(key, default)
        if val is None:
            return 0
        val = val.strip().lower()
        if val in ("on", "t", "true", "yes", "0"):
            return 1
        if val in ("off", "f", "false", "no", "1"):
            return 0
        self.error('Unrecognized boolean: key="%s", value="%s"' % \
                   (key, val))
        return 0


    def normalize_path(self, path):
        prefix = os.path.normpath(os.path.join(self.__root, self.__offset))
        path = os.path.normpath(path)
        
        if os.path.isabs(path):
            if path[:len(prefix)] != prefix:
                self.error("Invalid path: %s" % path)
            return path

        assert self.__root is not None
        assert self.__offset is not None
        
        path = os.path.join(prefix, path)

        return path


    def relative_path(self, path):

        # If this looks like a full URL, let it pass through.
        if path[:5] == "http:":
            return path
        
        if os.path.isabs(path):
            if path[:len(self.__root)] != self.__root:
                self.error("Invalid path: %s" % path)
            path = path[len(self.__root)+1:]
            path = os.path.normpath(path)
            return path

        assert self.__offset is not None

        path = os.path.join(self.__offset, path)
        path = os.path.normpath(path)

        return path
