###
### oc_serverconf.py
###
### Copyright (C) 2003, 2004 Ximian, Inc.
###
### This program is free software; you can redistribute it and/or
### modify it under the terms of version 2 of the GNU General Public
### License as published by the Free Software Foundation.
###
### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.
### 
### You should have received a copy of the GNU General Public License
### along with this program; if not, write to the Free Software
### Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
### USA.

import glob, os, string, sys, gzip
import oc_conffile, oc_channelconf

class ServerConf(oc_conffile.ConfFile):

    def __init__(self):
        oc_conffile.ConfFile.__init__(self)

        self.add_known_key("Name", required=1)
        self.add_known_key("ServerID", required=1)
        self.add_known_key("ContactEmail", required=1)
        self.add_known_key("DistsFile", required=0)
        self.add_known_key("MirrorsFile", required=0)
        self.add_known_key("LicensesFile", required=0)
        self.add_known_key("NewsFile", required=0)


    def pre_process(self):
        self.__all_channels = []


    def process_addchannel(self, val):

        channel_path = self.normalize_path(val)
        matches = glob.glob(channel_path)

        if not matches:
            self.warning("Ignoring invalid path in AddChannel.")
            return

        for filename in matches:
            if os.path.exists(filename) and os.access(filename, os.R_OK):
                if os.path.isdir(filename):
                    self.process_addchannel(os.path.join(filename, "*.conf"))
                else:
                    c = oc_channelconf.ChannelConf()
                    c.read(filename, parent=self)

                    self.__all_channels.append(c)
                    
            else:
                self.error("Can't read file '%s'" % filename)


    def post_process(self):
        self.validate_required_keys()

        serviceinfo_xml_target = self.normalize_path("serviceinfo.xml")
        fh = open(serviceinfo_xml_target, "wb")

        fh.write('<?xml version="1.0"?>')
        fh.write('\n<service ')
        fh.write('name="%s" unique_id="%s" contact_email="%s" '
                 'channels_file="channels.xml.gz"' %
                 (self.get("Name"), self.get("ServerID"),
                  self.get("ContactEmail")))
        
        if self.get("DistsFile"):
            fh.write(' distributions_file="%s"' % self.get("DistsFile"))
        if self.get("MirrorsFile"):
            fh.write(' mirrors_file="%s"' % self.get("MirrorsFile"))
        if self.get("LicensesFile"):
            fh.write(' licenses_file="%s"' % self.get("LicensesFile"))
        if self.get("NewsFile"):
            fh.write(' news_file="%s"' % self.get("NewsFile"))

        fh.write("/>\n")
        fh.close()

        channels_xml_target = self.normalize_path("channels.xml.gz")
        fh = gzip.open(channels_xml_target, "wb")

        fh.write('<?xml version="1.0"?>')
        fh.write('\n<channellist>\n')
        for c in self.__all_channels:
            s = string.join(c.get_channel_xml(), "\n")
            fh.write(s)
        fh.write('\n</channellist>\n')
        fh.close()

                    
        
