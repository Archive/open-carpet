
import sys, string, os, stat
import redcarpet

def get_packages(path, packman=None, recursive=1):

    if not packman:
        packman = redcarpet.Packman()

    packages = []
    
    for filename in os.listdir(path):
        filename = os.path.join(path, filename)
        if os.path.isdir(filename):
            if recursive:
                packages.extend(get_packages(filename, packman))
        else:
            pkg = packman.query_file(filename)
            if pkg:
                pkg.add_dummy_update(filename,
                                     os.stat(filename)[stat.ST_SIZE])
                packages.append(pkg)
                

    return packages
